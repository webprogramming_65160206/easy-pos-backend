import { Role } from 'src/roles/entities/role.entity';
import { CreateUserDto } from './create-user.dto';
import { PartialType } from '@nestjs/mapped-types';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  id: number;

  email: string;

  fullName: string;

  password: string;

  gender: string;

  roles: string;
}
