import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;

    const saltOrRounds = 10;
    user.password = await bcrypt.hash(createUserDto.password, saltOrRounds);

    // แปลงJSON เป็น object
    user.roles = JSON.parse(createUserDto.roles);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }

    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      select: {
        email: true,
        password: true,
        gender: true,
        roles: true,
      },
      where: { email },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.password = updateUserDto.password;
    // แปลงJSON เป็น object
    user.roles = JSON.parse(updateUserDto.roles);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    console.log(updateUserDto.roles);
    console.log(updateUser.roles);

    for (const r of user.roles) {
      const searchRole = updateUser.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        // ถ้า update มาแล้วหาไม่เจอ ในdatabase ก็ให้ push เข้าไปเพิ่ม
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      // ถ้าหาที่ส่งมาไม่เจอแล้วให้ลบ ให้ลบในที่ฝั่งหลังบ้าน
      const index = user.roles.findIndex(
        (role) => role.id === updateUser.roles[i].id,
      );
      if (index < 0) {
        updateUser.roles.splice(i, 1);
      }
    }
    await this.usersRepository.save(updateUser);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
